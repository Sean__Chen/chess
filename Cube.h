#pragma once
class Cube
{
public:
	Cube(int x, int y);
	~Cube();
	int getX() const;
	int getY() const;
	bool operator==(Cube& other) const;
	Cube& operator=(const Cube& other);
	void setX(int x) { this->_x = x; }
	void setY(int y) { this->_y = y; }


private:
	int _x;
	int _y;
};

