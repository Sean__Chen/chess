#include "Bishop.h"



Bishop::Bishop(PieceName type, char color):Piece(type,color)
{
	this->_color = color;
	this->_type = type;
}


Bishop::~Bishop()
{
}

bool Bishop::isValidMove(Cube& src, Cube& dest, Piece* b[WIDTH][LENGTH]) const
{
	if (abs(dest.getX() - src.getX()) == abs(dest.getY() - dest.getY()))
	{
		if (dest.getY() - src.getY() > 0) {
			for (int i = 0; i < abs(dest.getY() - src.getX()); i++)
			{
				if (b[src.getX() + i][src.getY() + i] != nullptr)
				{
					return false;
				}
			}
		}
		else {
			for (int i = abs(dest.getY() - src.getX()); i > 0 ; i--)
			{
				if (b[src.getX() - i][src.getY() - i] != nullptr)
				{
					return false;
				}
			}
		}
	}
	else
	{
		return false;
	}
	return true;
}
