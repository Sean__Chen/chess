#pragma once
#include <iostream>
#include "Cube.h"
#define WIDTH 8
#define LENGTH 8
using std::string;

enum PieceName
{
	RookName, BishopName, QueenName, KingName, KnightName, PawnName
};


class Piece
{

public:
	Piece(PieceName type, char color) : color(color), type(type) {}
	~Piece() {}
	virtual bool isValidMove(Cube& src, Cube& dest, Piece* b[WIDTH][LENGTH]) const = 0;
	virtual PieceName getType() const = 0;
	virtual char getColor() const = 0;

protected:
	char color;
	PieceName type;
};











