#include "Pawn.h"
#define STEP_BLACK 1
#define FIRST_STEP_BLACK 2
#define FIRST_STEP_WHITE -2
#define STEP_WHITE -1

bool Pawn::isValidMove(Cube& src, Cube& dest, Piece* b[WIDTH][LENGTH]) const
{
	//begining raws
	const int beg_white_pawn_raw = 7;
	const int beg_black_pawn_raw = 1;

	if (dest.getX() == src.getX())
	{
		//TODO: eating
		if (b[src.getY()][src.getX()]->getColor() == 'b')
		{
			if (dest.getY() - src.getY() != STEP_BLACK)
			{
				return false;
			}
			else if (dest.getY() - src.getY() != FIRST_STEP_BLACK || src.getY() != beg_black_pawn_raw)
			{
				return false;
			}
		}
		else
		{
			if (dest.getY() - src.getY() != STEP_WHITE)
			{
				return false;
			}
			else if (dest.getY() - src.getY() != FIRST_STEP_WHITE || src.getY() != beg_white_pawn_raw)
			{
				return false;
			}
		}
	}
	else
	{
		return false;
	}
	return true;
	
}