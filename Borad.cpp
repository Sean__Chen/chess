#include "Borad.h"



Borad::Borad(string& data, char player):_WhiteKing(Cube(0,0)), _BlackKing(Cube(0, 0)) {
	this->player = player;
	int index = 0;
	for (int i = 0; i < 8; ++i) 
	{
		for (int j = 0; j < 8; ++j) 
		{
			switch (data[index])
			{
				case 'r':
					this->board[i][j] = new Rook(RookName, 'b');
					break;
				case 'R':
					this->board[i][j] = new Rook(RookName, 'w');
					break;
				case 'b':
					this->board[i][j] = new Bishop(BishopName, 'b');
					break;
				case 'B':
					this->board[i][j] = new Bishop(BishopName, 'w');
					break;
				case 'q':
					this->board[i][j] = new Queen(QueenName, 'b');
					break;
				case 'Q':
					this->board[i][j] = new Queen(QueenName, 'w');
					break;
				case 'n':
					this->board[i][j] = new Knight(KnightName, 'b');
					break;
				case 'N': 
					this->board[i][j] = new Knight(KnightName, 'w');
					break;
				case 'p':
					this->board[i][j] = new Pawn(PawnName, 'b');
					break;
				case 'P':
					this->board[i][j] = new Pawn(PawnName, 'w');
					break;
				case 'k':
					this->board[i][j] = new King(KingName, 'b');
					this->_BlackKing = Cube(i, j);
					break;
				case 'K':
					this->board[i][j] = new King(KingName, 'w');
					this->_WhiteKing = Cube(i,j);
					break;
				case '#':
					this->board[i][j] = nullptr;
					break;
				default:
					break;
			}
			index++;
		}
	}
	//saving the board
	for (int i = 0; i < LENGTH; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{
			tempBoard[i][j] = board[i][j];
		}
	}
}


Borad::~Borad()
{
	for (int i = 0; i < LENGTH; i++)
	{
		for (int j = 0; j < WIDTH; j++) {
			delete this->board[i][j];
		}
	}
}

char Borad::isValidMove(string& data)
{
	Cube src = getCube(data.substr(0, 2));
	Cube dest = getCube(data.substr(2, 2));
	// checking that there is a piece in the src cube and the piece that the plays
	//is moving is his piece
	if (this->board[src.getY()][src.getX()] == nullptr || 
		this->board[src.getY()][src.getX()]->getColor() != this->player)
	{
		return '2';
	}
	else if (this->board[dest.getX()][dest.getY()] != nullptr && this->player == this->board[src.getX()][src.getY()]->getColor())
	{
		return '3';
	}
	else if (src.getX() > LENGTH || src.getX() < 0 || src.getY() > WIDTH || src.getY() < 0 ||
		dest.getX() > LENGTH || dest.getX() < 0 || dest.getY() > WIDTH || dest.getY() < 0)
	{
		return '5';
	}
	else if (!this->board[src.getY()][src.getX()]->isValidMove(src, dest, this->board))
	{
		return '6';
	}
	else if (src == dest) {
		return '7';
	}

	this->updateBoard(src, dest);

	if (this->checkChess(_WhiteKing, 'b')) { // cheking the white king
		this->restoreBoards();
		if (this->player == 'b') {
			return '1';
		}
		else {
			return '4';
		}
	}
	else if (this->checkChess(_BlackKing, 'w')) { // checking the black king
		this->restoreBoards();
		if (this->player == 'w') {
			return '1';
		}
		else {
			return '4';
		}
	}
	else
	{
		this->updateKings(dest);
		this->setPlayer(this->player);
		return '0';
	}
	

}

void Borad::updateKings(Cube& dest)
{
	if (this->board[dest.getY()][dest.getX()]->getType() == KingName) {
		if (this->board[dest.getY()][dest.getX()]->getColor() == 'w') {
			this->_WhiteKing = dest;
		}
		else {
			this->_BlackKing = dest;
		}
	}
}



Cube Borad::getCube(string data) const
{
	int y = abs(data[1] - 56);//casting
	int x = abs(data[0] - 97);//casting
	Cube c = Cube(x, y);
	return c;
}


/*
	-the function being called if and only if 
     the move is valid
	-The function updates the board
	input: Cube src and Cube dest
	output: none
*/
void Borad::updateBoard(Cube& src, Cube& dest)
{
	for (int i = 0; i < LENGTH; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{
			tempBoard[i][j] = board[i][j];
		}
	}
	this->board[dest.getY()][dest.getX()] = this->board[src.getY()][src.getX()];//putting the src piece in the dest
	this->board[src.getY()][src.getX()] = nullptr;//make the src empty
}

/*
	printing the board
*/
void Borad::printBoard()
{
	for (int i = 0; i < WIDTH; i++)
	{
		for (int j = 0; j < LENGTH; j++)
		{
			if (this->board[i][j] == nullptr)
			{
				cout << "# ";
			}
			else
			{
				switch (this->board[i][j]->getType())
				{
				case RookName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "r ";
					}
					else
					{
						cout << "R ";
					}
					break;
				case BishopName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "b ";
					}
					else
					{
						cout << "B ";
					}
					break;
				case QueenName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "q ";
					}
					else
					{
						cout << "Q ";
					}
					break;
				case KingName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "k ";
					}
					else
					{
						cout << "K ";
					}
					break;
				case KnightName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "n ";
					}
					else
					{
						cout << "N ";
					}
					break;
				case PawnName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "p ";
					}
					else
					{
						cout << "P ";
					}
					break;
				default:
					cout << "invalid note!";
					break;
				}
			}
		}
		cout << endl;
	}
}


/*
	Arrives to the function if the move is valid
	Function switches the player
	input: the current color
	output: none
*/
void Borad::setPlayer(char color)
{
	if (color == 'b')
	{
		this->player = 'w';
	}
	else
	{
		this->player = 'b';
	}
}


bool Borad::checkChess(Cube& kingLocation, char color)
{
	Cube temp(0, 0);
	for (int i = 0; i < LENGTH; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{
			if (this->board[i][j] != nullptr)
			{
				if (this->board[i][j]->getColor() == color &&
					this->board[i][j]->isValidMove(temp, kingLocation, this->board))
				{
					return true;
				}
			}
		}
	}
	return false;
}

void Borad::restoreBoards() {
	for (int i = 0; i < WIDTH; i++)
	{
		for (int j = 0; j < LENGTH; j++)
		{
			this->board[i][j] = this->tempBoard[i][j];
		}
	}
}