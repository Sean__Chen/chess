#include "Rook.h"



Rook::Rook(PieceName type, char color):Piece(type,color)
{
	this->_color = color;
	this->_type = type;
}


Rook::~Rook()
{
}


bool Rook::isValidMove(Cube& src, Cube& dest, Piece* b[WIDTH][LENGTH]) const
{
	if (src.getX() == dest.getX())
	{
		for (int i = 0; i < abs(dest.getY() - src.getY()); i++)
		{
			if (b[src.getX()][i] != nullptr)
			{
				return false;
			}
		}
	}
	else if (src.getY() == dest.getY())
	{
		for (int i = 0; i < abs(src.getX() - dest.getX()); i++)
		{
			if (b[i][src.getY()] != nullptr)
			{
				return false;
			}
		}
	}
	else
	{
		return false;
	}
	return true;//if arrives to here ,everything is valid
}

