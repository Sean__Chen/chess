#pragma once
#include "Piece.h"
class Bishop :virtual public Piece
{
public:
	Bishop(PieceName type, char color);
	~Bishop();
	virtual bool isValidMove(Cube& src, Cube& dest, Piece* b[WIDTH][LENGTH]) const;
	virtual PieceName getType() const { return this->_type; }
	virtual char getColor() const { return this->_color; }

private:
	PieceName _type;
	char _color;
};

